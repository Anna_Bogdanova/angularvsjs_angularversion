var myApp = angular.module('myApp', []);

angular.module('myApp').directive('content', function() {
    return {
        restrict: 'E',
        template: `<p>{{a}}</p><p>{{b}}</p><p>{{text}}</p><input type="button" ng-click="onClick()">`,
        transclude: true,
        scope: {
            b: '=',
            text: '='
        },
        link: function (scope) {
            scope.a = 1;
            scope.b = 100;
            scope.text = "Welcome!";
            scope.onClick = function () {
                scope.a++;
            }
        }
    };
});

myApp.controller("MyController", function($scope, $timeout, $http){
    $timeout(function(){$scope.bbb++;
        $http({
            method: 'GET',
            url: 'https://baconipsum.com/api/?type=all-meat&paras=1'
        }).then(function successCallback(response) {
            $scope.newText=response.data[0];
        }, function errorCallback(response) {
        });}, 2000);

});